SELECT T3.type, T4.value
FROM (
    SELECT MAX(T1.id) id, T1.type
    FROM data T1
    INNER JOIN (
        SELECT type, MAX(date) max_date
        FROM data
        GROUP BY type
    ) T2
    ON T1.type = T2.type AND T1.date = T2.max_date
    GROUP BY T1.type
) T3
LEFT JOIN data T4
ON T3.id = T4.id
