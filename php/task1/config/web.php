<?php

return [
    'layout' => path_join(BASE_DIR, 'views', 'layouts', 'main.php'),

    'default_controller_name' => 'main',
    'default_action_name' => 'index',
];
