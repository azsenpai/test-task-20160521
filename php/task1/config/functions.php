<?php

function get_value(&$var, $default = null)
{
    return isset($var) ? $var : $default;
}

function safe_value($value)
{
    return htmlspecialchars($value);
}

function norm_path($path)
{
    return str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
}

function path_join()
{
    return implode(DIRECTORY_SEPARATOR, func_get_args());
}

function get_view_content($view, &$params)
{
    ob_start();

    require(path_join(BASE_DIR, 'views', norm_path($view) . '.php'));

    $output = ob_get_contents();
    ob_end_clean();

    return $output;
}

function render($view, $params = [])
{
    global $app;

    ob_start();

    $content = get_view_content($view, $params);
    require($app->layout);

    $output = ob_get_contents();
    ob_end_clean();

    return $output;
}
