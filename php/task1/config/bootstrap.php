<?php

define('BASE_DIR', dirname(__DIR__));

require('functions.php');

define('IMAGES_DIR', path_join(BASE_DIR, 'web', 'images'));

require(path_join(BASE_DIR, 'core', 'Application.php'));
require(path_join(BASE_DIR, 'core', 'controllers', 'Controller.php'));
