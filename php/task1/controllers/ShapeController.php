<?php

class ShapeController extends Controller
{
    public function actionDraw()
    {
        global $app;

        if (!$app->is_post()) {
            $app->redirect();
        }

        $shapes = $app->post('shapes');
        $types = get_value($shapes['type'], []);

        $params = get_value($shapes['params'], []);
        $images = [];

        foreach ($types as $i => $type) {
            $type = strtolower($type);

            switch ($type) {
                case 'circle':
                    $app->import_model('Circle');

                    $r = get_value($params['r'][$i], 150);
                    $obj = new Circle(75, 75, $r);

                    break;

                case 'square':
                    $app->import_model('Square');

                    $a = get_value($params['a'][$i], 150);
                    $obj = new Square(50, 50, 50 + $a, 50 + $a);

                    break;

                default:
                    $obj = null;
            }

            if ($obj) {
                $images[] = $obj->draw();
            }
        }

        return $this->render('draw', [
            'images' => $images,
        ]);
    }
}
