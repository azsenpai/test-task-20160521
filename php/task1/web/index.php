<?php

require(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'config', 'bootstrap.php']));

$config = require(path_join(BASE_DIR, 'config', 'web.php'));
$app = Application::getinstance($config);

$app->run();
