<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= get_value($params['title'], 'test task') ?></title>
        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <div class="wrapper">
            <?= $content ?>
        </div>
    </body>
</html>