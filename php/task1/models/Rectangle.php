<?php

global $app;

$app->import_model('Shape');

class Rectangle extends Shape
{
    public $x1;
    public $y1;

    public $x2;
    public $y2;

    public function __construct($x1 = 0, $y1 = 0, $x2 = 0, $y2 = 0)
    {
        parent::__construct();

        $this->norm_coord($x1, $y1);
        $this->norm_coord($x2, $y2);

        $this->x1 = $x1;
        $this->y1 = $y1;

        $this->x2 = $x2;
        $this->y2 = $y2;
    }

    public function draw()
    {
        $filename = $this->new_filename();

        $canvas = imagecreatetruecolor($this->canvas_size['width'], $this->canvas_size['height']);

        $color = imagecolorallocate($canvas, $this->color['R'], $this->color['G'], $this->color['B']);
        imagerectangle($canvas, $this->x1, $this->y1, $this->x2, $this->y2, $color);

        imagepng($canvas, path_join(IMAGES_DIR, $filename));
        imagedestroy($canvas);

        return $filename;
    }
}
