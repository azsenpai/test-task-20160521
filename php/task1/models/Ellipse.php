<?php

global $app;

$app->import_model('Shape');

class Ellipse extends Shape
{
    public $cx;
    public $cy;

    public $width;
    public $height;

    public function __construct($cx = 0, $cy = 0, $width = 0, $height = 0)
    {
        parent::__construct();

        $this->norm_coord($cx, $cy);

        $this->cx = $cx;
        $this->cy = $cy;

        $this->width = $width;
        $this->height = $height;
    }

    public function draw()
    {
        $filename = $this->new_filename();

        $canvas = imagecreatetruecolor($this->canvas_size['width'], $this->canvas_size['height']);

        $color = imagecolorallocate($canvas, $this->color['R'], $this->color['G'], $this->color['B']);
        imageellipse($canvas, $this->cx, $this->cy, $this->width, $this->height, $color);

        imagepng($canvas, path_join(IMAGES_DIR, $filename));
        imagedestroy($canvas);

        return $filename;
    }
}
