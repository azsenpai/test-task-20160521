<?php

global $app;

$app->import_model('Rectangle');

class Square extends Rectangle
{
    public function __construct($x1 = 0, $y1 = 0, $x2 = 0, $y2 = 0)
    {
        parent::__construct($x1, $y1, $x2, $y2);
    }
}
