<?php

global $app;

$app->import_model('Ellipse');

class Circle extends Ellipse
{
    public function __construct($cx = 0, $cy = 0, $width = 0)
    {
        parent::__construct($cx, $cy, $width, $width);
    }
}
