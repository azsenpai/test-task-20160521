<?php

class Shape
{
    public $color;
    public $canvas_size;

    public function __construct()
    {
        $this->color = [
            'R' => 255,
            'G' => 255,
            'B' => 255,
        ];

        $this->canvas_size = [
            'width' => 150,
            'height' => 150,
        ];
    }

    public function draw()
    {
        return null;
    }

    public function new_filename()
    {
        return strtolower(get_class($this)) . '-' . uniqid() . '.png';
    }

    public function norm_coord(&$x, &$y)
    {
        $x = (int) $x;
        $y = (int) $y;

        if ($x < 0) {
            $x = 0;
        }
        else if ($x >= $this->canvas_size['width']) {
            $x = $this->canvas_size['width'] - 1;
        }

        if ($y < 0) {
            $y = 0;
        }
        else if ($y >= $this->canvas_size['height']) {
            $y = $this->canvas_size['height'] - 1;
        }
    }
}
