class A:
    def __init__( self ):
        self.__value = 1
    def getvalue( self ):
        return self.__value

class B( A ):
    def __init__( self ):
        A.__init__( self )
        self.__value = 2

b = B()
print (b.getvalue() == b.__value)
