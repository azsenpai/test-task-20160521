class A:
    def __init__(self, name):
        self.name = name
    def __del__(self):
        print(self.name)

aa = [A(str(i)) for i in range(3)]

for a in aa:
    del a

print('done')
