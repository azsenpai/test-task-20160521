class A:
    def __init__(self):
        self._greeting = 'hello'

    def greet(self):
        print(self._greeting)

class B(A):
    def __init__(self):
        self._greeting_add = ', world'

    def greet(self):
        print (self._greeting + self._greeting_add)

b = B()
b.greet()
