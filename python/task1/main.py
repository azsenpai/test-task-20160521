import sys

def main():
    i_filename = 'input.txt'
    o_filename = 'output.txt'

    if len(sys.argv) == 2:
        i_filename = sys.argv[1]
    elif len(sys.argv) > 2:
        i_filename = sys.argv[1]
        o_filename = sys.argv[2]

    fi = open(i_filename, 'r')
    fo = open(o_filename, 'w')

    for line in fi:
        fo.write(line[-2::-1] + line[-1])

    fi.close()
    fo.close()

if __name__ == '__main__':
    main()
